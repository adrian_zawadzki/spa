#ifndef ASTTREE_H
#define ASTTREE_H
#include <list>
#include <memory>
#include <unordered_map>
#include <functional>
#include <ostream>
#include <istream>
#include <sstream>
#include "parseexception.h"
#include <iostream>

inline std::istream & removeSpacesFromStream(std::istream & is) {
    while(!isgraph(is.peek())){
        is.get();
    }
    return is;
}



class ASTTree {
    std::list<std::shared_ptr<ASTTree>> nodeList;
    ASTTree *parent;
public:
    ASTTree(ASTTree *parent);
    void addNode(std::shared_ptr<ASTTree> node);
    void addNodeToFront(std::shared_ptr<ASTTree> node){nodeList.push_front(node);}
    void setParent(ASTTree *parent){this->parent = parent;}
    std::string nodeName;
    std::list<std::shared_ptr<ASTTree> > &getNodesList();
    std::string getNodeName(){return nodeName;}
    friend std::ostream & operator<<(std::ostream &os,std::shared_ptr<ASTTree> tree){
        os << "<" << tree->nodeName << ">\n";
        for(auto elem : tree->nodeList) {
            os << elem;
        }
        os << "</" << tree->nodeName << ">\n";
        return os;
    }
};

class StmtList : public ASTTree {
public:
    StmtList(ASTTree *parent): ASTTree(parent){
        nodeName = "StmtList";
        std::cerr << nodeName << std::endl;
    }

};

class Identifier : public ASTTree {
public:
    virtual ~Identifier() = default;
    Identifier(ASTTree *parent) : ASTTree(parent){std::cerr <<  "iden" << std::endl;}
    friend std::istream & operator >>(std::istream & is, std::shared_ptr<Identifier> & obj) {
        obj->nodeName = "";
        removeSpacesFromStream(is);
        while(isalnum(is.peek())) {
            obj->nodeName += is.get();
        }
        if(!obj->nodeName.length() || !isalpha(obj->nodeName[0])) {
            parseException exception("invalid identifier:" + obj->nodeName);
            throw exception;
        }
        std::cerr << "Identifier: " << obj->nodeName << std::endl;
        return is;
    }
};

class Constant : public ASTTree {
public:
    Constant (ASTTree *parent) : ASTTree(parent){std::cerr << "const" << std::endl;}
    friend std::istream & operator >>(std::istream & is, std::shared_ptr<Constant> & obj) {
        obj->nodeName = "";
        removeSpacesFromStream(is);
        while(isdigit(is.peek())) {
            obj->nodeName += is.get();
        }
        if(!obj->nodeName.length()) {
            parseException exception("invalid constant: " + obj->nodeName);
            throw exception;
        }
        obj->nodeName = "constval_" + obj->nodeName;
        return is;
    }
};

class Factor : public ASTTree {
public:
    Factor(ASTTree *parent) : ASTTree(parent){
        nodeName = "factor";
        std::cerr << nodeName << std::endl;}
    friend std::istream & operator >>(std::istream & is, std::shared_ptr<Factor> & obj) {
        removeSpacesFromStream(is);
        char singleChar;
        while((singleChar = is.peek()) !=';') {
            if(singleChar == '-' || singleChar == '+' ){
                break;
            }
            if(singleChar == '*') {
                obj->signsString.push_back(singleChar);
                is.get();
            } if(isalpha(singleChar)) {
                std::shared_ptr<Identifier> iden(new Identifier(NULL));
                is >> iden;
                obj->exprList.push_back(iden);
            } if (isdigit(singleChar)) {
                std::shared_ptr<Constant> constant(new Constant(NULL));
                is >> constant;
                obj->exprList.push_back(constant);
            }
            removeSpacesFromStream(is);
        }
        std::shared_ptr<Factor> prevNode(nullptr);
        while(obj->signsString.size() > 1){
            std::shared_ptr<Factor> expr(new Factor(NULL));
            expr->addNode(obj->exprList.back());
            expr->nodeName = "sign_"+obj->signsString.back();
            obj->signsString.pop_back();
            obj->exprList.pop_back();
            if(prevNode != nullptr){
                prevNode->setParent(expr.get());
            }
            prevNode = expr;
        }
        if(prevNode != nullptr){
            prevNode->setParent(obj.get());
        }
        obj->nodeName = "sign_"+obj->signsString.back();
        obj->addNodeToFront(obj->exprList.front());
        return is;
    }
    std::list<std::shared_ptr<ASTTree>> exprList;
    std::list<char> signsString;
};

class Expression : public ASTTree {
public:
    Expression(ASTTree *parent) : ASTTree(parent){
        nodeName = "expression";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>(std::istream & is, std::shared_ptr<Expression> & obj) {
        removeSpacesFromStream(is);
        char singleChar;
        while((singleChar = is.peek()) !=';') {

            if(singleChar == '-' || singleChar == '+' ) {
                obj->signsString.push_back(singleChar);
                is.get();
            } else if(singleChar == '*') {
                std::shared_ptr<Factor> factor(new Factor(NULL));
                is >> factor;
                obj->exprList.push_back(factor);
            } if(singleChar == '(') {
                std::shared_ptr<Expression> expr(new Expression(NULL));
                obj->exprList.push_back(expr);
            } if(singleChar == ')'){
                return is;
            } if(isalpha(singleChar)) {
                std::shared_ptr<Identifier> iden(new Identifier(NULL));
                is >> iden;
                obj->exprList.push_back(iden);
            } if (isdigit(singleChar)) {
                std::shared_ptr<Constant> constant(new Constant(NULL));
                is >> constant;
                obj->exprList.push_back(constant);
            }
            if(singleChar == '\n'){
                parseException exception("expresson parse error: unexpected end of line");
                throw exception;
            }
            removeSpacesFromStream(is);
        }
        std::shared_ptr<Expression> prevNode(nullptr);
        std::cerr << obj->signsString.size() << std::endl;
         std::cerr << obj->exprList.size() << std::endl;
        while(obj->signsString.size() > 1) {
            std::cerr << "here" <<std::endl;
            std::shared_ptr<Expression> expr(new Expression(NULL));
            expr->addNode(obj->exprList.back());
            expr->nodeName = obj->signsString.back();
            obj->signsString.pop_back();
            obj->exprList.pop_back();
            if(prevNode != nullptr){
                prevNode->setParent(expr.get());
            }
            prevNode = expr;
        }
        if(prevNode!= nullptr){
            prevNode->setParent(obj.get());
            obj->addNode(prevNode);
        }
        if(obj->signsString.size())
            obj->nodeName = +obj->signsString.back();
        obj->addNodeToFront(obj->exprList.front());
        if(obj->exprList.size()){
            obj->addNodeToFront(obj->exprList.front());
        }
        return is;
    }

    std::list<std::shared_ptr<ASTTree>> exprList;
    std::list<char> signsString;
};


class Assign : public ASTTree {
public:
    Assign(ASTTree *parent): ASTTree(parent){
        nodeName = "Assign";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<Assign>& assign) {
        removeSpacesFromStream(is);
        char c;
        is >> c;
        if(c != '=') {
            std::cerr << "in = " << c << std::endl;
            parseException exception("expected '=' got: " );
            throw exception;
        }
        std::shared_ptr<Expression> exprNode(new Expression(assign.get()));
        exprNode->nodeName = "Expression";
        std::shared_ptr<Expression> expr(new Expression(exprNode.get()));
        is >> expr;
        removeSpacesFromStream(is);
        char semicolon = is.get();
        if(semicolon != ';'){
            parseException exception("expected ';'");
            throw exception;
        }
        exprNode->addNode(expr);
        assign->addNode(exprNode);
        return is;
    }
};


class Stmt : public ASTTree {
public:
    Stmt(ASTTree *parent): ASTTree(parent){
        nodeName = "Stmt";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<Stmt>& stmt) {
        std::string iden="";
        removeSpacesFromStream(is);
        while(isalnum(is.peek())) {
            iden+= is.get();
        }
        if(!iden.length() || !isalpha(iden[0])) {
            parseException exception("expected identifier. got: "+iden);
            throw exception;
        }
        if(stmt->nodeCreateMap.find(iden) != stmt->nodeCreateMap.end()) {
            std::shared_ptr<ASTTree> node = stmt->nodeCreateMap[iden](stmt.get(),is);
            stmt->addNode(node);
        } else {
            std::shared_ptr<Assign> node(new Assign(stmt.get()));
            std::stringstream sstream;
            sstream << iden;
            std::shared_ptr<Identifier> idenNode(new Identifier(node.get()));
            sstream >> idenNode;
            node->addNode(idenNode);
            is >> node;
            stmt->addNode(node);
        }
        return is;
    }
    static std::unordered_map<std::string,std::function<std::shared_ptr<ASTTree>(ASTTree *parent,std::istream & is)> > nodeCreateMap;
};

inline std::istream &operator >>(std::istream &is, std::shared_ptr<StmtList> &stmtList) {
    removeSpacesFromStream(is);
    if(removeSpacesFromStream(is).peek() != '{'){
        return is;
    }
    is.get();
    do{
        std::shared_ptr<Stmt> node(new Stmt(stmtList.get()));
        is >> node;
        stmtList->addNode(node);
    } while(removeSpacesFromStream(is).peek() != '}' );
    is.get();
    return is;
}


class Procedure : public ASTTree {
public:
    Procedure(ASTTree *parent) : ASTTree(parent){
        nodeName = "Procedure";
        std::cerr << nodeName << std::endl;
    }
    virtual ~Procedure() = default;
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<Procedure> proc) {
        std::string procedureWord;
        if(!(is >> procedureWord)){
            return is;
        }
        if(procedureWord != "procedure") {
            parseException exception("expected keyword: procedure, got: "+procedureWord);
            throw exception;
        }
        std::shared_ptr<Identifier> identifier(new Identifier (proc.get()));
        is >> identifier;
        proc->addNode(identifier);
        std::shared_ptr<StmtList> stmtList(new StmtList(proc.get()));
        is >> stmtList;
        proc->addNode(stmtList);
        return is;
    }
};



class Program : public ASTTree {
public:
    Program() : ASTTree(NULL){
        nodeName = "Program";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<Program>& program) {
        std::shared_ptr<Procedure> procedure(new Procedure(program.get()));
        while((is >> procedure) && is.peek() != EOF) {

            program->addNode(procedure);
            procedure.reset(new Procedure(program.get()));
        }
        return is;
    }

};

class While : public ASTTree {
public:
    While(ASTTree *parent): ASTTree(parent){
        nodeName = "While";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<While>& whileStmt) {
        removeSpacesFromStream(is);
        std::shared_ptr<Identifier> idenNode(new Identifier(whileStmt.get()));
        is >> idenNode;
        whileStmt->addNode(idenNode);
        std::shared_ptr<StmtList> whileStmtList(new StmtList(whileStmt.get()));
        is >> whileStmtList;
        whileStmt->addNode(whileStmtList);
        return is;
    }
};

class If : public ASTTree {
public:
    If(ASTTree *parent): ASTTree(parent){
        nodeName = "If";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<If>& ifStmt) {
        removeSpacesFromStream(is);
        std::shared_ptr<Identifier> idenNode(new Identifier(ifStmt.get()));
        is >> idenNode;
        ifStmt->addNode(idenNode);
        std::shared_ptr<Identifier> keywordIden(new Identifier(NULL));
        is >> keywordIden;
        if(keywordIden->getNodeName() != "then") {
            parseException exception("expected keyword: then");
            throw exception;
        }
        std::shared_ptr<StmtList> ifStmtList(new StmtList(ifStmt.get()));
        is >> ifStmtList;
        ifStmt->addNode(ifStmtList);
        is >> keywordIden;
        if(keywordIden->getNodeName() != "else") {
            parseException exception("expected keyword: else");
            throw exception;
        }
        std::shared_ptr<StmtList> elseStmtList(new StmtList(ifStmt.get()));
        is >> elseStmtList;
        return is;
    }
};

class Call : public ASTTree {
public:
    Call(ASTTree *parent): ASTTree(parent) {
        nodeName = "Call";
        std::cerr << nodeName << std::endl;
    }
    friend std::istream & operator >>( std::istream & is, std::shared_ptr<Call>& callStmt) {
        std::shared_ptr<Identifier> procedureIden(new Identifier(callStmt.get()));
        is >> procedureIden;
        callStmt->addNode(procedureIden);
        removeSpacesFromStream(is);
        char semicolon = is.get();
        if(semicolon !=';') {
            parseException exception("expected: ;" );
            throw exception;
        }
        return is;
    }
};



#endif // ASTTREE_H
