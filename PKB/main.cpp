
#include <fstream>
#include "asttree.h"
#include <iostream>

int main(int argc, char *argv[])
{
    std::ifstream in("in.txt");
    std::shared_ptr<Program> prog(new Program());
    try{
    in >> prog;
    }catch(parseException ex){
        std::cerr << "exception: " << ex.what() << std::endl;
    }

    std::ofstream out("out.txt");
    std::shared_ptr<ASTTree> tree;
    tree.reset(prog.get());
    out << tree;

    return 0;
}
