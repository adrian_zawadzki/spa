#-------------------------------------------------
#
# Project created by QtCreator 2016-03-10T16:08:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Parser
TEMPLATE = app


SOURCES += main.cpp\
        parserwindow.cpp \
    asttree.cpp

HEADERS  += parserwindow.h \
    asttree.h \
    parseexception.h

FORMS    += parserwindow.ui

CONFIG += mobility
MOBILITY = 

QMAKE_CXXFLAGS = -std=c++11
