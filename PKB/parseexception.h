#ifndef PARSEEXCEPTION_H
#define PARSEEXCEPTION_H
#include <string>


class parseException : public std::exception {
public:
    parseException(std::string message) {
        whatMessage = message;
    }
    std::string what() {
        return whatMessage;
    }
    std::string whatMessage;
};
#endif // PARSEEXCEPTION_H
