#include "asttree.h"
#include<list>
#include "parseexception.h"

class parseException;

ASTTree::ASTTree(ASTTree *parent) :parent(parent) {
}

std::list<std::shared_ptr<ASTTree> > &ASTTree::getNodesList(){
    return nodeList;
}

void ASTTree::addNode(std::shared_ptr<ASTTree> node) {
    nodeList.push_back(node);
}

std::unordered_map<std::string,std::function<std::shared_ptr<ASTTree>(ASTTree *parent,std::istream & is)> > Stmt::nodeCreateMap = {
    {"while", [](ASTTree *parent,std::istream & is) {
        std::shared_ptr<While> whileNode(new While(parent));
        is >> whileNode;
        return whileNode;
    }},
    {"if", [](ASTTree *parent,std::istream & is) {
            std::shared_ptr<If> ifNode(new If(parent));
            is >> ifNode;
            return ifNode;
        }},
    {"call", [](ASTTree *parent,std::istream & is) {
            std::shared_ptr<Call> callNode(new Call(parent));
            is >> callNode;
            return callNode;
        }}
    };
